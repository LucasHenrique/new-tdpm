import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductionComponent } from './production/production.component';

const productionRoutes: Routes = [
  {
    path: '',
    component: ProductionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(productionRoutes)],
  exports: [RouterModule]
})
export class ProductionRoutingModule {}

/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
