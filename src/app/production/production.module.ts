import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductionComponent } from './production/production.component';
import { ProductionRoutingModule } from './production-routing.module';
import { MatListModule, MatTabsModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatListModule,
    MatTabsModule,
    ProductionRoutingModule
  ],
  declarations: [ProductionComponent]
})
export class ProductionModule {}
