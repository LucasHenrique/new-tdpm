import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpecificationListComponent } from './specification-list/specification-list.component';
import { MatListModule, MatTabsModule } from '@angular/material';
import { SpecificationRoutingModule } from './installation-routing.module';

@NgModule({
  imports: [
    CommonModule,
    MatListModule,
    MatTabsModule,
    SpecificationRoutingModule
  ],
  declarations: [SpecificationListComponent]
})
export class SpecificationModule {}
