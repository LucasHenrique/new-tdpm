import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SpecificationListComponent } from './specification-list/specification-list.component';

const projectRoutes: Routes = [
  {
    path: '',
    component: SpecificationListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(projectRoutes)],
  exports: [RouterModule]
})
export class SpecificationRoutingModule {}

/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
