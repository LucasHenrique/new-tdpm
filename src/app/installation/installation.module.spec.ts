import { InstallationModule } from './installation.module';

describe('InstallationModule', () => {
  let installationModule: InstallationModule;

  beforeEach(() => {
    installationModule = new InstallationModule();
  });

  it('should create an instance', () => {
    expect(installationModule).toBeTruthy();
  });
});
