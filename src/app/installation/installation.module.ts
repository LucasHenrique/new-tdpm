import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InstallationComponent } from './installation/installation.component';
import { InstallationRoutingModule } from './installation-routing.module';
import { MatListModule, MatTabsModule } from '@angular/material';

@NgModule({
  imports: [CommonModule, MatTabsModule, InstallationRoutingModule],
  declarations: [InstallationComponent]
})
export class InstallationModule {}
