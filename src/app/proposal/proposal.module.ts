import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProposalListComponent } from './proposal-list/proposal-list.component';
import { ProposalRoutingModule } from './proposal-routing.module';
import { MatListModule, MatTabsModule } from '@angular/material';

@NgModule({
  imports: [CommonModule, MatTabsModule, ProposalRoutingModule],
  declarations: [ProposalListComponent]
})
export class ProposalModule {}
