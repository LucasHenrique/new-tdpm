import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './not-found/not-found.component';
import { MainComponent } from './main/main.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: MainComponent
      },
      {
        path: 'project',
        loadChildren: './project/project.module#ProjectModule'
      },
      {
        path: 'proposal',
        loadChildren: './proposal/proposal.module#ProposalModule'
      },
      {
        path: 'production',
        loadChildren: './production/production.module#ProductionModule'
      },
      {
        path: 'installation',
        loadChildren: './installation/installation.module#InstallationModule'
      },
      {
        path: 'specification',
        loadChildren: './specification/specification.module#SpecificationModule'
      }
    ]
  },

  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
