import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  LineChart = [];

  closedProjects = [
    'Abba',
    'Abbate',
    'Abbe',
    'Abbey',
    'Abbi',
    'Abbie',
    'Abbot',
    'Abbotsen',
    'Abbotson',
    'Abbotsun',
    'Hinson',
    'Hintze',
    'Hinze',
    'Hippel',
    'Hirai',
    'Hiram',
    'Hirasuna',
    'Hiro',
    'Hiroko',
    'Hiroshi',
    'Hirsch',
    'Hirs'
  ];

  clients = [
    'Abba',
    'Abbate',
    'Abbe',
    'Abbey',
    'Abbi',
    'Abbie',
    'Abbot',
    'Abbotsen',
    'Abbotson',
    'Abbotsun',
    'Hinson',
    'Hintze',
    'Hinze',
    'Hippel',
    'Hirai',
    'Hiram',
    'Hirasuna',
    'Hiro',
    'Hiroko',
    'Hiroshi',
    'Hirsch',
    'Hirs'
  ];

  constructor() {}

  ngOnInit() {
    this.loadInstallationChart();
    this.loadProductionChart();
  }
  loadProductionChart() {
    this.LineChart = new Chart('productionChart', {
      type: 'doughnut',
      data: {
        labels: ['In production', 'Production Queue'],
        datasets: [
          {
            label: 'Number of Projects',
            data: [243, 322],
            fill: true,
            backgroundColor: ['#63FF84', '#FF6384'],
            lineTension: 0.2,
            borderColor: 'white',
            borderWidth: 1
          }
        ]
      },
      options: {
        legend: {
          labels: {
            fontColor: 'black',
            fontFamily: 'Oxygen',
            fontSize: 18
          },
          position: 'left'
        }
      }
    });
  }
  loadInstallationChart() {
    this.LineChart = new Chart('installationChart', {
      type: 'doughnut',
      data: {
        labels: ['Installed', 'Installation Queue', 'Installation'],
        datasets: [
          {
            label: 'Number of Projects',
            data: [243, 322, 122],
            fill: true,
            backgroundColor: ['#63FF84', '#6384FF', '#FF6384'],
            lineTension: 0.2,
            borderColor: 'white',
            borderWidth: 1
          }
        ]
      },
      options: {
        legend: {
          labels: {
            fontColor: 'black',
            fontFamily: 'Oxygen',
            fontSize: 18
          },
          position: 'left'
        }
      }
    });
  }
}
